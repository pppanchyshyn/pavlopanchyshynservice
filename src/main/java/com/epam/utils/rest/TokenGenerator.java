package com.epam.utils.rest;

import java.math.BigInteger;
import java.util.Random;

public class TokenGenerator {

  public static String generateToken() {
    Random random = new Random();
    return new BigInteger(130, random).toString(32);
  }
}