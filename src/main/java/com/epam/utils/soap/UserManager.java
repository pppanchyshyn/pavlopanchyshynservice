package com.epam.utils.soap;

import com.epam.model.soap.User;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;

public class UserManager {

  private static String[] HEADERS = {"UserName", "Password", "Roles", "Online"};

  private static InputStreamReader newReader(final InputStream inputStream) {
    return new InputStreamReader(new BOMInputStream(inputStream), StandardCharsets.UTF_8);
  }

  public static List<User> readUserBase(File file) {
    List<User> userList = new ArrayList<>();
    try (InputStream in = new FileInputStream(file)) {
      Iterable<CSVRecord> parser = CSVFormat.DEFAULT.withHeader(HEADERS).withSkipHeaderRecord(true)
          .parse(newReader(in));
      parser.forEach(
          x -> userList.add(
              new User(x.get("UserName"), x.get("Password"), x.get("Roles"),
                  Boolean.valueOf(x.get("Online")))));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return userList;
  }

  public static void writeUserBase(List<User> userList, File file) {
    CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n").withNullString("");
    try (Writer out = new BufferedWriter(
        new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
        CSVPrinter printer = new CSVPrinter(out, csvFileFormat)) {
      printer.printRecord(HEADERS);
      for (User user : userList) {
        printer.printRecord(user.getName(), user.getPassword(), String.join(",", user.getRoles()),
            String.valueOf(user.isOnline()));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
