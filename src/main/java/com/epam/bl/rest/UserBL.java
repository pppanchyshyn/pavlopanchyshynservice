package com.epam.bl.rest;

import com.epam.dao.rest.UserDAO;
import com.epam.model.rest.User;
import java.util.List;
import java.util.stream.Collectors;

public class UserBL {

  public boolean createAccount(User user) {
    return UserDAO.createAccount(user);
  }

  public boolean signIn(User user) {
    return UserDAO.signIn(user);
  }

  public List<String> authorize(String token) {
    return UserDAO.authorize(token);
  }

  public List<User> getAllUsers() {
    return UserDAO.getAllUsers();
  }

  public List<User> getUsersByRole(String role) {
    List<User> userList = UserDAO.getAllUsers();
    return userList.stream()
        .filter(x -> x.getRoles().stream().anyMatch(y -> y.equals(role)))
        .collect(Collectors.toList());
  }

  public boolean deleteUser(String deletedUserName) {
    return UserDAO.deleteUser(deletedUserName);
  }

  public User getUserByName(String userName) {
    return UserDAO.getUserByName(userName);
  }

  public User getUserByToken(String token){
    return UserDAO.getUserByToken(token);
  }

  public boolean signOut(String token) {
    return UserDAO.signOut(token);
  }

  public boolean isAdmin(String userName) {
    return UserDAO.getUserByName(userName).getRoles().stream()
        .anyMatch(role -> role.equals("Admin"));
  }
}
