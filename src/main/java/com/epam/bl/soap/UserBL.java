package com.epam.bl.soap;

import com.epam.dao.soap.UserDAO;
import com.epam.model.soap.User;
import java.util.List;
import java.util.stream.Collectors;

public class UserBL {

  public boolean createAccount(User user) {
    return UserDAO.createAccount(user);
  }

  public boolean signIn(String userName, String password) {
    return UserDAO.signIn(userName, password);
  }

  public List<String> authorize(String username) {
    return UserDAO.authorize(username);
  }

  public List<User> getAllUsers() {
    return UserDAO.getAllUsers();
  }

  public List<User> getUsersByRole(String role) {
    List<User> userList = UserDAO.getAllUsers();
    return userList.stream()
        .filter(x -> x.getRoles().stream().anyMatch(y -> y.equals(role)))
        .collect(Collectors.toList());
  }

  public boolean deleteUser(String deletedUserName) {
    return UserDAO.deleteUser(deletedUserName);
  }

  public User getUserByName(String userName) {
    return UserDAO.getUserByName(userName);
  }

  public boolean signOut(String userName) {
    return UserDAO.signOut(userName);
  }

  public boolean isAdmin(String userName) {
    return UserDAO.getUserByName(userName).getRoles().stream()
        .anyMatch(role -> role.equals("Admin"));
  }
}
