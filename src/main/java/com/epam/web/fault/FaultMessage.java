package com.epam.web.fault;

public class FaultMessage {

  private FaultMessage() {
  }

  public static final String ILLEGAL_NAME = "User with same name already exists";
  public static final String WRONG_CREDENTIALS = "The name or password was entered incorrectly";
  public static final String NON_EXISTENT_NAME = "There is no user with specified name";
  public static final String USER_NOT_SIGN_IN = "Error. Sign in to complete this operation";
  public static final String ILLEGAL_OPERATION = "Error. This operation is only available to the administrator";
  public static final String USERS_NOT_FOUND = "There is no users found by specified role";
  public static final String ATTEMPT_DELETING_ADMIN = "Error. Admin can't be deleted";
  public static final String INVALID_TOKEN = "Error. Invalid token";
}
