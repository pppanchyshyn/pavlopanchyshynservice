package com.epam.web.fault;

public class FaultInfo {

  private String message;

  public FaultInfo(String message, Object... args) {
    this.message = String.format(message, args);
  }

  public FaultInfo(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
