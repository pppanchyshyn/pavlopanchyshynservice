package com.epam.web.soap.interfaces;

import com.epam.model.soap.User;
import java.util.List;
import javax.jws.WebService;

@WebService
public interface UserService {

  boolean createAccount(String userName, String password, String roles);

  boolean signIn(String userName, String password);

  List<String> authorize(String userName);

  List<User> getAllUsers(String userName);

  List<User> getUsersByRole(String userName, String role);

  boolean deleteUser(String userName, String deletedUserName);

  boolean signOut(String userName);
}
