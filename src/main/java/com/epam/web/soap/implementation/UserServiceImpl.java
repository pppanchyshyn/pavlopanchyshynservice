package com.epam.web.soap.implementation;

import com.epam.bl.soap.UserBL;
import com.epam.web.fault.FaultInfo;
import com.epam.web.fault.FaultMessage;
import com.epam.model.soap.User;
import com.epam.web.soap.exception.ServiceException;
import com.epam.web.soap.interfaces.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.jws.WebService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

@WebService(endpointInterface = "com.epam.web.soap.interfaces.UserService")
public class UserServiceImpl implements UserService {

  private Logger logger = LogManager.getLogger(UserServiceImpl.class);
  private static final String LINE = "------------------------------------------------------------";

  @Override
  public boolean createAccount(String userName, String password, String roles) {
    logger.info(LINE);
    logger.info(String.format("Creating of new account for user %s", userName));
    UserBL userBL = new UserBL();
    User user = new User(userName, password, roles);
    boolean isCreated;
    if (!userBL.createAccount(user)) {
      isCreated = false;
      FaultInfo info = new FaultInfo(FaultMessage.ILLEGAL_NAME, user.getName());
      try {
        throw new ServiceException(info);
      } catch (ServiceException e) {
        logger.warn(FaultMessage.ILLEGAL_NAME);
      }
    } else {
      isCreated = true;
      logger.info("Account created successfully");
    }
    return isCreated;
  }

  @Override
  public boolean signIn(String userName, String password) {
    logger.info(LINE);
    logger.info(String.format("%s try to sign in...", userName));
    UserBL userBL = new UserBL();
    boolean isSignIn = true;
    if (!userBL.signIn(userName, password)) {
      isSignIn = false;
      FaultInfo info = new FaultInfo(FaultMessage.WRONG_CREDENTIALS);
      try {
        throw new ServiceException(info);
      } catch (ServiceException e) {
        logger.warn(String.format("Fail. %s", FaultMessage.WRONG_CREDENTIALS));
      }
    } else {
      logger.info(String.format("%s sign in successfully", userName));
    }
    return isSignIn;
  }

  @Override
  public List<String> authorize(String userName) {
    logger.info(LINE);
    logger.info(String.format("Authorization for %s...", userName));
    UserBL userBL = new UserBL();
    List<String> userRoles = null;
    if (isUserExisted(userName)) {
      if (isUserOnline(userName)) {
        userRoles = userBL.authorize(userName);
        logger.info(String.format("%s is authorized successfully", userName));
      }
    }
    return userRoles;
  }

  @Override
  public List<User> getAllUsers(String userName) {
    logger.info(LINE);
    logger.info(String.format("%s try to get list of all users...", userName));
    UserBL userBL = new UserBL();
    List<User> userList = new ArrayList<>();
    if (isUserExisted(userName)) {
      if (isUserOnline(userName)) {
        if (isOperationAvailableForCurrentUser(userName)) {
          userList = userBL.getAllUsers();
          logger.info(String.format("Data is available for %s", userName));
        }
      }
    }
    return userList;
  }

  @Override
  public List<User> getUsersByRole(String userName, String role) {
    logger.info(LINE);
    logger.info(String.format("%s try to get list users with role %s...", userName, role));
    UserBL userBL = new UserBL();
    List<User> userListByRole = null;
    if (isUserExisted(userName)) {
      if (isUserOnline(userName)) {
        if (isOperationAvailableForCurrentUser(userName)) {
          userListByRole = userBL.getUsersByRole(role);
          if (userListByRole.isEmpty()) {
            FaultInfo info = new FaultInfo(FaultMessage.USERS_NOT_FOUND, role);
            try {
              throw new ServiceException(info);
            } catch (ServiceException e) {
              logger.warn(FaultMessage.USERS_NOT_FOUND);
            }
          } else {
            logger.info(String.format("Data is available for %s", userName));
          }
        } else {
          userListByRole = null;
        }
      }
    }
    return userListByRole;
  }

  @Override
  public boolean deleteUser(String userName, String deletedUserName) {
    logger.info(LINE);
    logger.info(String.format("%s try to delete user '%s'...", userName, deletedUserName));
    UserBL userBL = new UserBL();
    boolean isDeleted = false;
    if (isUserExisted(userName)) {
      if (isUserOnline(userName)) {
        if (isOperationAvailableForCurrentUser(userName)) {
          if (isUserExisted(deletedUserName)) {
            if (!userName.equals(deletedUserName)) {
              isDeleted = userBL.deleteUser(deletedUserName);
              logger.info(String.format("%s's account successfully deleted", deletedUserName));
            } else {
              FaultInfo info = new FaultInfo(FaultMessage.ATTEMPT_DELETING_ADMIN);
              try {
                throw new ServiceException(info);
              } catch (ServiceException e) {
                logger.warn(FaultMessage.ATTEMPT_DELETING_ADMIN);
              }
            }
          }
        }
      }
    }
    return isDeleted;
  }

  @Override
  public boolean signOut(String userName) {
    logger.info(LINE);
    logger.info(String.format("%s try to sign out...", userName));
    UserBL userBL = new UserBL();
    boolean isSignOut = false;
    if (isUserExisted(userName)) {
      if (isUserOnline(userName)) {
        if (userBL.signOut(userName)) {
          isSignOut = true;
          logger.info(String.format("%s sign out successfully", userName));
        } else {
          FaultInfo info = new FaultInfo(FaultMessage.USER_NOT_SIGN_IN);
          try {
            throw new ServiceException(info);
          } catch (ServiceException e) {
            logger.warn(String.format("Fail. %s", FaultMessage.USER_NOT_SIGN_IN));
          }
        }
      }
    }
    return isSignOut;
  }

  private boolean isOperationAvailableForCurrentUser(String userName) {
    logger.info(String.format("Checking admin status for user %s...", userName));
    UserBL userBL = new UserBL();
    boolean isAvailable = true;
    if (!userBL.isAdmin(userName)) {
      logger.info("Admin status not verified");
      isAvailable = false;
      FaultInfo info = new FaultInfo(FaultMessage.ILLEGAL_OPERATION);
      try {
        throw new ServiceException(info);
      } catch (ServiceException e) {
        logger.warn(FaultMessage.ILLEGAL_OPERATION);
      }
    } else {
      logger.info("Admin status confirmed");
    }
    return isAvailable;
  }

  private boolean isUserExisted(String userName) {
    logger.info(String.format("Searching for user %s...", userName));
    UserBL userBL = new UserBL();
    boolean isExisted = true;
    if (Objects.isNull(userBL.getUserByName(userName))) {
      isExisted = false;
      FaultInfo info = new FaultInfo(FaultMessage.NON_EXISTENT_NAME, userName);
      try {
        throw new ServiceException(info);
      } catch (ServiceException e) {
        logger.warn(FaultMessage.NON_EXISTENT_NAME);
      }
    } else {
      logger.info(String.format("%s found on the system", userName));
    }
    return isExisted;
  }

  private boolean isUserOnline(String userName) {
    logger.info(String.format("Check whether the %s is signed in...", userName));
    UserBL userBL = new UserBL();
    boolean isOnline = true;
    User user = userBL.getUserByName(userName);
    if (!user.isOnline()) {
      isOnline = false;
      FaultInfo info = new FaultInfo(FaultMessage.USER_NOT_SIGN_IN);
      try {
        throw new ServiceException(info);
      } catch (ServiceException e) {
        logger.warn(FaultMessage.USER_NOT_SIGN_IN);
      }
    } else {
      logger.info(String.format("%s is signed in", userName));
    }
    return isOnline;
  }
}