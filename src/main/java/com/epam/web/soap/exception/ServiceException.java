package com.epam.web.soap.exception;

import com.epam.web.fault.FaultInfo;
import javax.xml.ws.WebFault;


@WebFault(name = "UserServiceFault")
public class ServiceException extends Exception {

  private FaultInfo faultInfo;

  public ServiceException(FaultInfo faultInfo) {
    this.faultInfo = faultInfo;
  }

  public FaultInfo getFaultInfo() {
    return faultInfo;
  }
}
