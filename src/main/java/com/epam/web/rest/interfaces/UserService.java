package com.epam.web.rest.interfaces;

import com.epam.model.rest.User;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/UsersBaseREST")
public interface UserService {

  @POST
  @Path("/user")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces("application/json; charset=UTF-8")
  Response createAccount(User user);

  @POST
  @Path("/authentication")
  @Consumes("application/json; charset=UTF-8")
  @Produces("text/plain; charset=UTF-8")
  Response signIn(User user);

  @GET
  @Path("/user/roles")
  @Produces("application/json; charset=UTF-8")
  Response authorize(@HeaderParam("token") String token);

  @GET
  @Path("/users")
  @Produces("application/json; charset=UTF-8")
  Response getAllUsers(@HeaderParam("token") String token);

  @GET
  @Path("/users/roles")
  @Consumes("text/plain; charset=UTF-8")
  @Produces("application/json; charset=UTF-8")
  Response getUsersByRole(@HeaderParam("token") String token, @QueryParam("role") String role);

  @DELETE
  @Path("/users")
  @Consumes("text/plain; charset=UTF-8")
  @Produces("application/json; charset=UTF-8")
  Response deleteUser(@HeaderParam("token") String token, @QueryParam("name") String deletedName);

  @GET
  @Path("/signOut")
  @Consumes("text/plain; charset=UTF-8")
  @Produces("application/json; charset=UTF-8")
  Response signOut(@HeaderParam("token") String token);
}
