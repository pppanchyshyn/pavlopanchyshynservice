package com.epam.web.rest.implementation;

import com.epam.bl.rest.UserBL;
import com.epam.model.rest.User;
import com.epam.web.fault.FaultMessage;
import com.epam.web.rest.interfaces.UserService;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class UserServiceImpl implements UserService {

  private Logger logger = LogManager.getLogger(UserServiceImpl.class);
  private static final String LINE ="------------------------------------------------------------";

  @Override
  public Response createAccount(User user) {
    logger.info(LINE);
    logger.info(String.format("Creating of new account for user %s", user.getName()));
    UserBL userBL = new UserBL();
    Response response;
    if (!userBL.createAccount(user)) {
      response = fail(Status.NOT_ACCEPTABLE, FaultMessage.ILLEGAL_NAME);
    } else {
      response = Response.ok().build();
      logger.info("Account created successfully");
    }
    return response;
  }

  @Override
  public Response signIn(User user) {
    logger.info(LINE);
    logger.info(String.format("%s try to sign in...", user.getName()));
    UserBL userBL = new UserBL();
    Response response;
    if (!userBL.signIn(user)) {
      response = fail(Status.UNAUTHORIZED, FaultMessage.WRONG_CREDENTIALS);
    } else {
      response = Response.ok(userBL.getUserByName(user.getName()).getToken()).build();
      logger.info(String.format("%s sign in successfully", user.getName()));
    }
    return response;
  }

  @Override
  public Response authorize(String token) {
    logger.info(LINE);
    logger.info(String.format("Authorization for user with token %s...", token));
    UserBL userBL = new UserBL();
    Response response;
    User user = userBL.getUserByToken(token);
    if (Objects.nonNull(user)) {
      response = Response.ok(userBL.authorize(token)).build();
      logger.info(String.format("%s is authorized successfully", user.getName()));
    } else {
      response = fail(Status.UNAUTHORIZED, FaultMessage.INVALID_TOKEN);
    }
    return response;
  }

  @Override
  public Response getAllUsers(String token) {
    logger.info(LINE);
    logger.info(String.format("User with token %s try to get list of all users...", token));
    UserBL userBL = new UserBL();
    Response response;
    User user = userBL.getUserByToken(token);
    if (Objects.nonNull(user)) {
      if (userBL.isAdmin(user.getName())) {
        response = Response.ok(userBL.getAllUsers()).build();
        logger.info(String.format("Data is available for %s", user.getName()));
      } else {
        response = fail(Status.FORBIDDEN, FaultMessage.ILLEGAL_OPERATION);
      }
    } else {
      response = fail(Status.UNAUTHORIZED, FaultMessage.INVALID_TOKEN);
    }
    return response;
  }

  @Override
  public Response getUsersByRole(String token, String role) {
    logger.info(LINE);
    logger
        .info(String.format("User with token %s try to get users with role %s...", token, role));
    UserBL userBL = new UserBL();
    Response response;
    User user = userBL.getUserByToken(token);
    List<User> userListByRole;
    if (Objects.nonNull(user)) {
      if (userBL.isAdmin(user.getName())) {
        userListByRole = userBL.getUsersByRole(role);
        if (!userListByRole.isEmpty()) {
          response = Response.ok(userListByRole).build();
          logger.info(String.format("Data is available for %s", user.getName()));
        } else {
          response = fail(Status.NO_CONTENT, FaultMessage.USERS_NOT_FOUND);
        }
      } else {
        response = fail(Status.FORBIDDEN, FaultMessage.ILLEGAL_OPERATION);
      }
    } else {
      response = fail(Status.UNAUTHORIZED, FaultMessage.INVALID_TOKEN);
    }
    return response;
  }

  @Override
  public Response deleteUser(String token, String deletedUserName) {
    logger.info(LINE);
    logger.info(String.format("User with token %s try to delete %s...", token, deletedUserName));
    UserBL userBL = new UserBL();
    Response response;
    User user = userBL.getUserByToken(token);
    if (Objects.nonNull(user)) {
      if (userBL.isAdmin(user.getName())) {
        if (Objects.nonNull(userBL.getUserByName(deletedUserName))) {
          if (!user.getName().equals(deletedUserName)) {
            response = Response.ok(userBL.deleteUser(deletedUserName)).build();
            logger.info(String.format("%s's account successfully deleted", deletedUserName));
          } else {
            response = fail(Status.FORBIDDEN, FaultMessage.ATTEMPT_DELETING_ADMIN);
          }
        } else {
          response = fail(Status.NOT_ACCEPTABLE, FaultMessage.NON_EXISTENT_NAME);
        }
      } else {
        response = fail(Status.FORBIDDEN, FaultMessage.ILLEGAL_OPERATION);
      }
    } else {
      response = fail(Status.UNAUTHORIZED, FaultMessage.INVALID_TOKEN);
    }
    return response;
  }

  public Response signOut(String token) {
    logger.info(LINE);
    logger.info(String.format("User with token %s try to sign out...", token));
    UserBL userBL = new UserBL();
    Response response;
    User user = userBL.getUserByToken(token);
    if (Objects.nonNull(user)) {
      response = Response.ok(userBL.signOut(token)).build();
      logger.info(String.format("%s sign out successfully", user.getName()));
    } else {
      response = fail(Status.UNAUTHORIZED, FaultMessage.INVALID_TOKEN);
    }
    return response;
  }

  private Response fail(Status status, String faultMessage) {
    logger.warn(faultMessage);
    return Response.status(status).build();
  }
}
