package com.epam.dao.soap;

import com.epam.model.soap.User;
import com.epam.utils.soap.UserManager;
import java.io.File;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class UserDAO {

  private static final String USERS_URl = "src/main/resources/UsersSOAP.csv";
  private static List<User> userList = UserManager.readUserBase(new File(USERS_URl));

  public static boolean createAccount(User user) {
    boolean isCreated = false;
    if (Objects.isNull(getUserByName(user.getName()))) {
      userList.add(user);
      UserManager.writeUserBase(userList, new File(USERS_URl));
      isCreated = true;
    }
    return isCreated;
  }

  public static boolean signIn(String userName, String password) {
    User user = new User(userName, password);
    User userInBase = getUserByName(userName);
    boolean isSignIn = user.equals(userInBase);
    if (isSignIn) {
      userInBase.setOnline(true);
      UserManager.writeUserBase(userList, new File(USERS_URl));
    }
    return isSignIn;
  }

  public static List<String> authorize(String username) {
    return getUserByName(username).getRoles();
  }

  public static List<User> getAllUsers() {
    return userList;
  }

  public static boolean deleteUser(String deletedUserName) {
    boolean isDeleted = false;
    ListIterator<User> iterator = userList.listIterator();
    if (!Objects.isNull(getUserByName(deletedUserName))) {
      while (iterator.hasNext()) {
        User userInBase = iterator.next();
        if (userInBase.getName().equals(deletedUserName)) {
          iterator.remove();
          UserManager.writeUserBase(userList, new File(USERS_URl));
          isDeleted = true;
          break;
        }
      }
    }
    return isDeleted;
  }

  public static boolean signOut(String userName) {
    User user = getUserByName(userName);
    user.setOnline(false);
    UserManager.writeUserBase(userList, new File(USERS_URl));
    return !user.isOnline();
  }

  public static User getUserByName(String userName) {
    return userList.stream().filter(user -> user.getName().equals(userName)).findFirst()
        .orElse(null);
  }
}