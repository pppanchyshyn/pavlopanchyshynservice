package com.epam.dao.rest;

import com.epam.model.rest.User;
import com.epam.utils.rest.TokenGenerator;
import com.epam.utils.rest.UserManager;
import java.io.File;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class UserDAO {

  private static final String USERS_URl = "src/main/resources/UsersREST.csv";
  private static List<User> userList = UserManager.readUserBase(new File(USERS_URl));

  public static boolean createAccount(User user) {
    boolean isCreated = false;
    if (Objects.isNull(getUserByName(user.getName()))) {
      userList.add(user);
      UserManager.writeUserBase(userList, new File(USERS_URl));
      isCreated = true;
    }
    return isCreated;
  }

  public static boolean signIn(User user) {
    User userInBase = getUserByName(user.getName());
    boolean isSignIn = user.equals(userInBase);
    if (isSignIn) {
      userInBase.setToken(TokenGenerator.generateToken());
      UserManager.writeUserBase(userList, new File(USERS_URl));
    }
    return isSignIn;
  }

  public static List<String> authorize(String token) {
    return getUserByToken(token).getRoles();
  }

  public static List<User> getAllUsers() {
    return userList;
  }

  public static boolean deleteUser(String deletedUserName) {
    boolean isDeleted = false;
    ListIterator<User> iterator = userList.listIterator();
    if (!Objects.isNull(getUserByName(deletedUserName))) {
      while (iterator.hasNext()) {
        User userInBase = iterator.next();
        if (userInBase.getName().equals(deletedUserName)) {
          iterator.remove();
          UserManager.writeUserBase(userList, new File(USERS_URl));
          isDeleted = true;
          break;
        }
      }
    }
    return isDeleted;
  }

  public static boolean signOut(String token) {
    User user = getUserByToken(token);
    user.setToken(null);
    UserManager.writeUserBase(userList, new File(USERS_URl));
    return Objects.isNull(user.getToken());
  }

  public static User getUserByName(String userName) {
    return userList.stream().filter(user -> user.getName().equals(userName)).findFirst()
        .orElse(null);
  }

  public static User getUserByToken(String token) {
    return userList.stream().filter(user -> user.getToken().equals(token)).findFirst()
        .orElse(null);
  }
}