package com.epam.model.soap;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class User {

  private String name;
  private String password;
  private List<String> roles;
  private Boolean isOnline;

  public User(String name, String password) {
    this.name = name;
    this.password = password;
  }

  public User(String name, String password, String roles) {
    this.name = name;
    this.password = password;
    this.roles = Arrays.asList(roles.split("\\s*,\\s*"));
    this.isOnline = false;
  }

  public User(String name, String password, String roles, boolean isOnline) {
    this.name = name;
    this.password = password;
    this.roles = Arrays.asList(roles.split("\\s*,\\s*"));
    this.isOnline = isOnline;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(List<String> roles) {
    this.roles = roles;
  }

  public Boolean isOnline() {
    return isOnline;
  }

  public void setOnline(Boolean isOnline) {
    this.isOnline = isOnline;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(name, user.name) &&
        Objects.equals(password, user.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, password);
  }

  @Override
  public String toString() {
    return String
        .format("User Name: %-10sPassword: %-10sRoles: %-40sOnline: %b", name, password, roles,
            isOnline);
  }
}
